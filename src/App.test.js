import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('renders commit link', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/latest demo app commit id: /i);
  expect(linkElement).toBeInTheDocument();
});
