# Demo App

## Environment setup

After you clone the project,<br>
In the project directory, you can simply run the command:

```bash
vagrant up --color
```

When machine will be up & running, check the app in browser using localhost:<br>
#### [http://127.0.0.1](http://127.0.0.1/)

or using VM's IP:

```bash
vagrant ssh -c "hostname -I | cut -d' ' -f2"
```
