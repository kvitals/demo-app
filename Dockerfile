FROM node:14-alpine AS build
ARG commitId
ENV REACT_APP_CI_COMMIT_SHA=$commitId
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --no-optional --no-progress
COPY . .
RUN npm test -- --watchAll=false \
 && npm run build


FROM nginx:alpine AS runtime
COPY --from=build /usr/src/app/build /usr/share/nginx/html

